// https://stackoverflow.com/questions/6156501/read-a-file-one-line-at-a-time-in-node-js
require('./config/config')
require('./db/mongoose')
const HTML5ToPDF = require('html5-to-pdf')
const {readFileSync} = require('fs')
const path = require('path')
const schema = require('./db/schemas/statement')
const {create} = require('./db/helperDb')

const run = async () => {
  const html5ToPDF = new HTML5ToPDF({
    inputPath: path.join(__dirname, 'assets', 'PlantillaHTML-CreditoConsumo.html'),
    outputPath: path.join(__dirname, 'tmp', 'output.pdf')
    // templatePath: path.join(__dirname, 'templates', 'basic'),
    // include: [
    //   path.join(__dirname, 'assets', 'basic.css'),
    //   path.join(__dirname, 'assets', 'custom-margin.css'),
    // ],
  })

  await html5ToPDF.start()
  await html5ToPDF.build()
  await html5ToPDF.close()
  // console.log('DONE')
  // process.exit(0)
  return readFileSync(path.join(__dirname, 'tmp', 'output.pdf'))

}

const newBankStatement = (data, contentType) => {
  return new schema({
    img: {
      data,
      contentType
    }
  })
}

run().then(buffer => {
  const statement = newBankStatement(buffer, 'image/pdf')
  console.log(create(statement))
}).catch(error => {
  console.log(error)
})