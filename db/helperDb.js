const db_helper = {
  create (model) {
    model.save().then(result => {
      if (result.errors) {
        return console.log('Error on create', result.errors)
      }

      return result._doc
    })
  }
}

module.exports = db_helper