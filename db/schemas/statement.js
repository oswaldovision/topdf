const mongoose = require('mongoose')
const Schema = mongoose.Schema

const statement = new Schema({
  img: {
    data: Buffer,
    contentType: String
  }
})

module.exports = mongoose.model('Statement', statement)