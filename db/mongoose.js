const mongoose = require('mongoose')

mongoose.Promise = global.Promise

mongoose.connect(`${process.env.MONGODB_URI}/${process.env.DATABASE_NAME}`, {
  useNewUrlParser: true,
  useCreateIndex: true
}).then(con => {
  console.log('Success database connection.' + con.connections[0].host + ' : ' + con.connections[0].name)
}).catch(error => {
  console.log('Error connecting to database! ', error)
})

module.exports = {mongoose}